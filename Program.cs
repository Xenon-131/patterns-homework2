﻿using System;
namespace Hotel
{
  public abstract class KitchenWorker
  {
    protected KitchenWorker successor;
    public void SetSuccessor(KitchenWorker successor)
    {
      this.successor = successor;
    }
    public abstract void ProcessOrder(FoodOrder order);
  }

  public class Barista : KitchenWorker
  {
    public override void ProcessOrder(FoodOrder order)
    {
      if (order.Type == "drink")
      {
        Console.WriteLine($"\t>{this.GetType().Name} cooked order№ {order.Number} ({order.Dish_name}) for room№ {order.Room}");
      }
      else if (successor != null)
      {
        successor.ProcessOrder(order);
      }
    }
  }

  public class AssistantCook : KitchenWorker
  {
    public override void ProcessOrder(FoodOrder order)
    {
      if (order.Type == "simpleDish")
      {
        Console.WriteLine($"\t>{this.GetType().Name} cooked order№ {order.Number} ({order.Dish_name}) for room№ {order.Room}");
      }
      else if (successor != null)
      {
        successor.ProcessOrder(order);
      }
    }
  }

  public class SeniorChef : KitchenWorker
  {
    public override void ProcessOrder(FoodOrder order)
    {
    if (order.Type == "specialDish")
    {
        Console.WriteLine($"\t>{this.GetType().Name} cooked order№ {order.Number} ({order.Dish_name}) for room№ {order.Room}");
      }
    else
      {
        Console.WriteLine(
            $"\t>Order№ {order.Number} ({order.Dish_name}) for room№ {order.Room} requires a main chef's presence!");
      }
    }
  }

  public class Room
  {
    int number;
    public Room(int number)
    {
      this.number = number;
    }

    public int Number
    {
      get { return number; }
      set { number = value; }
    }
  }


  public class FoodOrder
  {
    int number;
    string dish_name;
    string type;
    int room;


    public FoodOrder(int number, string dish_name, int room)
    {
      this.number = number;
      this.dish_name = dish_name;
      if (dish_name == "coffee" || dish_name == "cocktail")
      {
        this.type = "drink";
      }
      else if (dish_name == "pancake" || dish_name == "salad")
      {
        this.type = "simpleDish";
      }
      else if (dish_name == "steak" || dish_name == "crab")
      {
        this.type = "specialDish";
      }

      this.room = room;
    }

    public int Number
    {
      get { return number; }
      set { number = value; }
    }

    public string Dish_name
    {
      get { return dish_name; }
      set { dish_name = value; }
    }
    public string Type
    {
      get { return type; }
      set { type = value; }
    }
    public int Room
    {
      get { return number; }
      set { number = value; }
    }
  }

  public class FoodService
  {
    KitchenWorker michael = new Barista();
    KitchenWorker jean = new AssistantCook();
    KitchenWorker jeremy = new SeniorChef();

    public FoodService()
    {
      michael.SetSuccessor(jean);
      jean.SetSuccessor(jeremy);
    }

    public void CookFood(FoodOrder order)
    {
      michael.ProcessOrder(order);
    }

    public void DeliverIceCream(Room room)
    {
      Console.WriteLine(
            $"\t>IceCream delivered to room№ {room.Number}.");
    }
  }

  public class RoomService
  {
    public void CleanRoom(Room room)
    {
      Console.WriteLine(
            $"\t>Room№ {room.Number} cleaned.");
    }

    public void ChangeTowels(Room room)
    {
      Console.WriteLine(
            $"\t>Towels in room№ {room.Number} changed.");
    }
  }

  // Фасад
  public class CustomerService
  {
    FoodService foodService;
    RoomService roomService;
    private static CustomerService instance;

    protected CustomerService()
    {
      Console.WriteLine("CustomerService counstructor called");
      foodService = new FoodService();
      roomService = new RoomService();
    }

    public static CustomerService getCustomerService()
    {
      if ( instance == null)
      {
        instance = new CustomerService();
        Console.WriteLine("CustomerService instance created");
      }
      return instance;
    }

    public void CleanRoom(Room room)
    {
      Console.WriteLine();
      Console.WriteLine($"Client from room№ {room.Number} accessed CustomerService with request to clean room.");
      Console.WriteLine($"\tCustomerService sends request from room№ {room.Number} to RoomService");
      roomService.CleanRoom(room);
    }
    public void ChangeTowels(Room room)
    {
      Console.WriteLine();
      Console.WriteLine($"Client from room№ {room.Number} accessed CustomerService with request to change towels.");
      Console.WriteLine($"\tCustomerService sends request from room№ {room.Number} to RoomService");
      roomService.ChangeTowels(room);
    }

    public void DeliverIceCream(Room room)
    {
      Console.WriteLine();
      Console.WriteLine($"Client from room№ {room.Number} accessed CustomerService with request to deliver icecream.");
      Console.WriteLine($"\tCustomerService sends request from room№ {room.Number} to FoodService");
      foodService.DeliverIceCream(room);
    }
    public void CookFood(FoodOrder order)
    {
      Console.WriteLine();
      Console.WriteLine($"Client from room№ {order.Room} accessed CustomerService with request to cook food ({order.Dish_name})");
      Console.WriteLine($"\tCustomerService sends request from room№ {order.Room} to FoodService");
      foodService.CookFood(order);
    }

  }

  public class Program
  {
    public static void Main(string[] args)
    {
      CustomerService customerService = CustomerService.getCustomerService();
      CustomerService customerService2 = CustomerService.getCustomerService();
      if (customerService == customerService2)
      {
        Console.WriteLine("customerService and customerService2 are the same instance");
      }


      Room room1 = new Room(1);
      Room room2 = new Room(2);
      Room room3 = new Room(3);

      FoodOrder order1 = new FoodOrder(1 ,"coffee", room1.Number);
      customerService.CookFood(order1);
      customerService.DeliverIceCream(room1);
      FoodOrder order2 = new FoodOrder(2, "steak", room3.Number);
      customerService.CookFood(order2);
      FoodOrder order3 = new FoodOrder(3, "pancake", room2.Number);
      customerService.CookFood(order3);
      FoodOrder order4 = new FoodOrder(4, "Peking Duck", room1.Number);
      customerService.CookFood(order4);

      customerService.CleanRoom(room2);
      customerService.CleanRoom(room3);
      customerService.ChangeTowels(room3);

      Console.ReadKey();
    }
  }
}